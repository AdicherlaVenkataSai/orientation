<h1>Author: [VenkataSai Adicherla](https://adicherlavenkatasai.github.io/)</h1>

Note: First lets go through few theory then we'll do some hands on work to understand git.

<img align='right' src="https://media.giphy.com/media/M9gbBd9nbDrOTu1Mqx/giphy.gif" width="230">

## Whats Git?      
1. `Distributed` - Doesn't require a constant connection to a central server.       
2. `Adaptive` - Git's branching model can adapt to fit the workflow of almost any team.      
3. `Fast & Relible` - Git branches are sparse allowing for change-only tracking. 

## Terms to be Remembered 
`Repository`, `Fork`, `Clone`, `Commit`, `Push`, `Merge`, `Branch`     

## Workflow     
**Here the flow from a contributor side(till pushing the code):**
-  We first fork the master repository using the `fork` option.
-  Now we can clone the repository to local machine using the `git clone https://gitlab/AdicherlaVenkataSai/orientation.git` or use the `clone` option.
-  The repository is available on the local machine named as `orientation`.
-  Make changes to the repository.
-  Initiate the git `git init`
-  We can also check the status(about all changes) `git status`
-  Add the files which you want to push `git add .` this `.` period indicates all the created/edited/delete files.
-  Now we can commit the files with some meaningful message `git commit -m "Git Basics Updates" `.
-  Now add the origin `git add remote origin https://gitlab/AdicherlaVenkataSai/orientation.git`
-  It's time to push the code `git push -u origin master`

![testooo](https://user-images.githubusercontent.com/26376075/93762286-df19ec00-fc2c-11ea-8287-cf74826030a9.gif)

-  After pushing the code, we can create a `pull request` with few `comments` of what changes we have done.
-  `Maintainer`/ `Owner` of the repository `review` the changes and `merge` it to master repository.


![mering](/extras/gitlab4.png)

## References
-  [Git-ing started with Git](https://www.youtube.com/watch?v=Ce5nz5n41z4)
-  [Images](https://gitlab.com/AdicherlaVenkataSai/orientation/-/tree/master/extras)


<img src="https://media.giphy.com/media/LnQjpWaON8nhr21vNW/giphy.gif" width="60"> <em><b>I'd love to connect with different people. Send me 👋 i'll be happy to meet you</b> 😊</em>

**Can reach me out:**
[![Whatsapp: venkatasaiadicherla](https://img.shields.io/badge/-venkatasaiadicherla-%2325D366.svg?&flat-square&logo=whatsapp&logoColor=white&link=https://wa.me/+918008527755)](https://wa.me/+918008527755)
[![Linkedin: venkatasaiadicherla](https://img.shields.io/badge/-venkatasaiadicherla-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/adicherlavenkatasai/)](https://www.linkedin.com/in/adicherlavenkatasai/)
